# [FRB/US Project](https://www.federalreserve.gov/econres/us-models-about.htm) 用の Python 環境を Ubuntu/Debian 上で 用意する。

## 準備

1. Ubuntu か Debian のマシンを 1 台用意する。 (WSL でもコンテナでも VM でも OK)
1. 必要なパッケージをインストールする。
   ```sh
   sudo apt install libsuitesparse-dev swig unzip
   ```
1. [公式サイト](https://www.federalreserve.gov/econres/us-models-python.htm)から必要なデータをダウンロードして解凍する。  
   (下記の例では、`FRB`というフォルダを作成し、その中にファイルをダウンロードしています。)
   ```sh
   mkdir FRB
   cd FRB
   wget https://www.federalreserve.gov/econres/files/pyfrbus.zip
   unzip pyfrbus.zip
   wget https://www.federalreserve.gov/econres/files/data_only_package.zip
   unzip data_only_package.zip
   rm *.zip
   ```
1. Python パッケージをインストールする。  
   (下記の例では、`pyfrbus.zip`を解凍してできた`pyfrbus`というフォルダの名前を`pyfrb`に変更しています。  
   これは`ModuleNotFoundError: No module named 'pyfrbus.frbus'`というエラーを避けるためです。)
   ```sh
   sudo apt install python3 python3-pip
   pip install --upgrade pip
   pip install numpy
   mv pyfrbus pyfrb
   cd pyfrb
   pip3 install -e .
   ```
1. エラーの発生を避けるため、`scikits/umfpack/__init__.py`を修正する。
   1. `scikits/umfpack/__init__.py`を開く。
      ```sh
      ls ~/.local/lib/  # 👉 インストールされているPythonのバージョンを確認
      nano ~/.local/lib/python3.11/site-packages/scikits/umfpack/__init__.py  # 👉 バージョンに合わせて"3.11"の部分を変更
      ```
   1. 開いたファイルの以下の部分を
      ```python:$HOME/.local/lib/python3.xx/site-packages/scikits/umfpack/__init__.py
      from numpy.testing import Tester
      test = Tester().test
      ```
      以下の通りコメントアウトする。
      ```python:$HOME/.local/lib/python3.xx/site-packages/scikits/umfpack/__init__.py
      # from numpy.testing import Tester
      # test = Tester().test
      ```
      保存して終了は、`Ctrl+S` + `Ctrl+X`。

## サンプルコード実行

上記の準備が完了すると、`pyfrbus`パッケージが使えるようになります。  
サンプルコードを実行してみましょう。

1. サンプルコードの作成

   ```sh
   cd ..  # FRB/pyfrb から FRBに移動
   nano sample.py
   ```

   以下の内容をコピー＆ペースト

   ```python:sample.py
   from pyfrbus.frbus import Frbus
   from pyfrbus.load_data import load_data


   frbus = Frbus("pyfrb/models/model.xml")
   data = load_data("data_only_package/LONGBASE.TXT")

   start = "2019Q4"
   end = "2030Q4"
   baseline_with_adds = frbus.init_trac(start, end, data)
   print(baseline_with_adds)

   sim = frbus.solve(start, end, baseline_with_adds)
   print(sim)
   ```

   `Ctrl+S` + `Ctrl+X`で、保存して終了。

1. サンプルコード実行

   ```sh
   python3 sample.py
   ```

   10 秒程度で表のようなものが表示されると思います。

---

## 補足

1. FRB/US Project の公式サイトは 👉[ここ](https://www.federalreserve.gov/econres/us-models-about.htm)です。
1. pyFRB/US のドキュメントは、[FRB/US Python package (ZIP)](https://www.federalreserve.gov/econres/files/pyfrbus.zip)を解凍したフォルダの中の、`doc`フォルダの中にあります。  
   ファイルをダウンロードして内容を確認してください。
   - [index.html](https://gl.githack.com/e-271828/frbus/raw/main/docs/index.html)
   - [frbus.html](https://gl.githack.com/e-271828/frbus/raw/main/docs/frbus.html)
   - [py-modindex.html](https://gl.githack.com/e-271828/frbus/raw/main/docs/py-modindex.html)
   - [time_series.html](https://gl.githack.com/e-271828/frbus/raw/main/docs/time_series.html)
   - [user_guide.html](https://gl.githack.com/e-271828/frbus/raw/main/docs/user_guide.html) 👈 まずはこれ
   - [\_modules/index.html](https://gl.githack.com/e-271828/frbus/raw/main/docs/_modules/index.html)
   - [\_modules/pyfrbus/frbus.html](https://gl.githack.com/e-271828/frbus/raw/main/docs/_modules/pyfrbus/frbus.html)
1. `scikits/umfpack/__init__.py`を修正する理由は、  
   バージョン`1.18`以上の Numpy パッケージでは`numpy.testing.nosetester`が廃止されていますが、  
   `scikit-umfpack`パッケージではその機能を参照(使用)しようとするためです。
1. FRB/US を使用して、以下のような分析がなされています。
   - [東京大学エコノミックコンサルティング株式会社](https://utecon.net/pressrelease_20220701_1/)  
     👉 [2022 年 7 月レポート(PDF)](http://utecon.net/wp-content/uploads/2022/07/20220630_beta_final_jp.pdf)
   - [農林中金総合研究所](https://www.nochuri.co.jp/report/?f_first_realm=norin&f_first_realm_detail=overseas-finance&view_num1=200&view_num2=200#search_result)  
     👉 [2023 年 6 月レポート(PDF)](https://www.nochuri.co.jp/genba/pdf/forcus20230616-01.pdf) ,
     [2023 年 3 月レポート(PDF)](https://www.nochuri.co.jp/genba/pdf/forcus20220329-02.pdf) ,
     [2023 年 1 月レポート(PDF)](https://www.nochuri.co.jp/report/pdf/f2301ar2.pdf) ,  
     [2022 年 10 月レポート(PDF)](https://www.nochuri.co.jp/report/pdf/f2210ar2.pdf) ,
     [2022 年 7 月レポート(PDF)](https://www.nochuri.co.jp/report/pdf/f2207ar2.pdf) ,
     [2022 年 5 月レポート(PDF)](https://www.nochuri.co.jp/report/pdf/f2205ar2.pdf) ,
     [2021 年 12 月レポート(PDF)](https://www.nochuri.co.jp/report/pdf/f2112ar2.pdf)
   - [ニッセイ基礎研究所](https://www.nli-research.co.jp/report_tag/tag_id=85?site=nli)  
     👉 [2016 年 7 月レポート](https://www.nli-research.co.jp/report/detail/id=53295?site=nli)
1. FRB/US を有償の EViews ではなく、R で実行するための手順をまとめた[FRBUSinR](https://github.com/proudindiv/FRBUSinR/tree/master)というページがあります。  
   ここの 👉 [frbus.pdf](https://docs.google.com/viewer?url=https://raw.githubusercontent.com/proudindiv/FRBUSinR/ReverseEngineer/frbus.pdf)は、
   FRB/US で使用されている式や変数や考え方について非常に詳しくまとめられています。
